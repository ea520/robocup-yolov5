# Setup information
```
git clone https://gitlab.developers.cam.ac.uk/ea520/robocup-yolov5.git --recurse-submodules 
```

If you want more freedom to delete the following python modules, you can install a virtual environment ([instructions here](https://docs.python.org/3/tutorial/venv.html))

Ubuntu terminal:
```shell
cd ./robocup-yolov5/python-classifier/
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
```

Windows command prompt:
```
cd robocup-yolov5\python-classifier\
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
```

If you get any errors installing the requirements, try removing the version number of the problematic packages from requirements.txt  
e.g. in requirements.txt, replace, "opencv-python==4.1.2.30" with "opencv-python"

Programmes that actually do something are
- [inferer.py](python-classifier/inferer.py)
- [inference_ui.py](python-classifier/inference_ui.py)

Datasets can be found here:
https://drive.google.com/drive/folders/1qXHPDdkr82a3264u0dLkSJKe3rv2Av99?usp=sharing
Results can be found here:
https://drive.google.com/drive/folders/1-RydNbgLyefEdq3XzzkGZ3VZKOgcd_xD?usp=sharing
