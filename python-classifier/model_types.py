from enum import Enum, auto

class model_type(Enum):
    PERSON = auto()
    DOOR = auto()
    HAZMAT = auto()
    FIRE_EXTINGUISHER = auto()
    QR = auto()