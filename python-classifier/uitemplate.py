import pathlib
import pygubu
import tkinter as tk
import tkinter.ttk as ttk

PROJECT_PATH = pathlib.Path(__file__).parent
PROJECT_UI = PROJECT_PATH / "ui-template.ui"


class UiTemplateApp:
    def __init__(self, master=None):
        # build ui
        self.toplevel1 = tk.Tk() if master is None else tk.Toplevel(master)
        self.img_frame = ttk.Frame(self.toplevel1)
        self.img_label = ttk.Label(self.img_frame)
        self.img_label.configure(anchor='center')
        self.img_label.pack(anchor='w', expand='true', fill='both', side='top')
        self.img_frame.configure(height='480', width='640')
        self.img_frame.pack(side='left')
        self.labelframe = ttk.Labelframe(self.toplevel1)
        self.frame4 = ttk.Frame(self.labelframe)
        self.label2 = ttk.Label(self.frame4)
        self.label2.configure(text='Label')
        self.label2.grid(column='0', row='0')
        self.frame4.columnconfigure('0', pad='15')
        self.label3 = ttk.Label(self.frame4)
        self.label3.configure(text='Last inference time')
        self.label3.grid(column='1', row='0')
        self.frame4.columnconfigure('1', pad='15')
        self.label4 = ttk.Label(self.frame4)
        self.label4.configure(text='Desired inference interval')
        self.label4.grid(column='2', row='0')
        self.frame4.columnconfigure('2', pad='15')
        self.label5 = ttk.Label(self.frame4)
        self.label5.configure(text='Person')
        self.label5.grid(column='0', row='1')
        self.label6 = ttk.Label(self.frame4)
        self.person_time_txt = tk.StringVar(value='000.0 ms')
        self.label6.configure(text='000.0 ms', textvariable=self.person_time_txt)
        self.label6.grid(column='1', row='1')
        self.label7 = ttk.Label(self.frame4)
        self.person_interval_txt = tk.StringVar(value='000.0 ms')
        self.label7.configure(text='000.0 ms', textvariable=self.person_interval_txt)
        self.label7.grid(column='2', row='1')
        self.label20 = ttk.Label(self.frame4)
        self.label20.configure(text='Door')
        self.label20.grid(column='0', row='2')
        self.label21 = ttk.Label(self.frame4)
        self.door_time_txt = tk.StringVar(value='000.0 ms')
        self.label21.configure(text='000.0 ms', textvariable=self.door_time_txt)
        self.label21.grid(column='1', row='2')
        self.label22 = ttk.Label(self.frame4)
        self.door_interval_txt = tk.StringVar(value='000.0 ms')
        self.label22.configure(text='000.0 ms', textvariable=self.door_interval_txt)
        self.label22.grid(column='2', row='2')
        self.label23 = ttk.Label(self.frame4)
        self.label23.configure(text='Hazmat')
        self.label23.grid(column='0', row='3')
        self.label24 = ttk.Label(self.frame4)
        self.hazmat_time_txt = tk.StringVar(value='000.0 ms')
        self.label24.configure(text='000.0 ms', textvariable=self.hazmat_time_txt)
        self.label24.grid(column='1', row='3')
        self.label25 = ttk.Label(self.frame4)
        self.hazmat_interval_txt = tk.StringVar(value='000.0 ms')
        self.label25.configure(text='000.0 ms', textvariable=self.hazmat_interval_txt)
        self.label25.grid(column='2', row='3')
        self.label26 = ttk.Label(self.frame4)
        self.label26.configure(text='Fire extinguisher')
        self.label26.grid(column='0', row='4')
        self.label27 = ttk.Label(self.frame4)
        self.fire_time_txt = tk.StringVar(value='000.0 ms')
        self.label27.configure(text='000.0 ms', textvariable=self.fire_time_txt)
        self.label27.grid(column='1', row='4')
        self.label28 = ttk.Label(self.frame4)
        self.fire_interval_txt = tk.StringVar(value='000.0 ms')
        self.label28.configure(text='000.0 ms', textvariable=self.fire_interval_txt)
        self.label28.grid(column='2', row='4')
        self.label1 = ttk.Label(self.frame4)
        self.label1.configure(text='QR code')
        self.label1.grid(column='0', row='5')
        self.label8 = ttk.Label(self.frame4)
        self.qr_time_txt = tk.StringVar(value='000.0 ms')
        self.label8.configure(text='000.0 ms', textvariable=self.qr_time_txt)
        self.label8.grid(column='1', row='5')
        self.label9 = ttk.Label(self.frame4)
        self.qr_interval_txt = tk.StringVar(value='000.0 ms')
        self.label9.configure(text='000.0 ms', textvariable=self.qr_interval_txt)
        self.label9.grid(column='2', row='5')
        self.frame4.configure(height='200', width='200')
        self.frame4.pack(anchor='n', fill='x', side='top')
        self.labelframe.configure(height='240', text='Inference debug information', width='200')
        self.labelframe.pack(anchor='n', fill='x', ipadx='5', ipady='5', padx='5', pady='5', side='top')
        self.labelframe3 = ttk.Labelframe(self.toplevel1)
        self.frame3 = ttk.Frame(self.labelframe3)
        self.label30 = ttk.Label(self.frame3)
        self.label30.configure(text='Person')
        self.label30.grid(column='0', row='1')
        self.scale2 = ttk.Scale(self.frame3)
        self.person_thresh = tk.DoubleVar(value=0.7)
        self.scale2.configure(from_='0', length='200', orient='horizontal', to='1')
        self.scale2.configure(value='0.7', variable=self.person_thresh)
        self.scale2.grid(column='1', row='1')
        self.frame3.columnconfigure('1', pad='20')
        self.scale2.configure(command=self.on_person_conf)
        self.label36 = ttk.Label(self.frame3)
        self.label36.configure(text='Door')
        self.label36.grid(column='0', row='2')
        self.scale3 = ttk.Scale(self.frame3)
        self.door_thresh = tk.DoubleVar(value=0.7)
        self.scale3.configure(from_='0', length='200', orient='horizontal', to='1')
        self.scale3.configure(value='0.7', variable=self.door_thresh)
        self.scale3.grid(column='1', row='2')
        self.scale3.configure(command=self.on_door_conf)
        self.label37 = ttk.Label(self.frame3)
        self.label37.configure(text='Hazmat')
        self.label37.grid(column='0', row='3')
        self.scale4 = ttk.Scale(self.frame3)
        self.hazmat_thresh = tk.DoubleVar(value=0.7)
        self.scale4.configure(from_='0', length='200', orient='horizontal', to='1')
        self.scale4.configure(value='0.7', variable=self.hazmat_thresh)
        self.scale4.grid(column='1', row='3')
        self.scale4.configure(command=self.on_hazmat_conf)
        self.label38 = ttk.Label(self.frame3)
        self.label38.configure(text='Fire Extinguisher')
        self.label38.grid(column='0', row='4')
        self.scale5 = ttk.Scale(self.frame3)
        self.fire_thresh = tk.DoubleVar(value=0.7)
        self.scale5.configure(from_='0', length='200', orient='horizontal', to='1')
        self.scale5.configure(value='0.7', variable=self.fire_thresh)
        self.scale5.grid(column='1', row='4')
        self.scale5.configure(command=self.on_fire_conf)
        self.label39 = ttk.Label(self.frame3)
        self.person_thresh_txt = tk.StringVar(value='0.70')
        self.label39.configure(text='0.70', textvariable=self.person_thresh_txt)
        self.label39.grid(column='2', row='1')
        self.label40 = ttk.Label(self.frame3)
        self.door_thresh_txt = tk.StringVar(value='0.70')
        self.label40.configure(text='0.70', textvariable=self.door_thresh_txt)
        self.label40.grid(column='2', row='2')
        self.label41 = ttk.Label(self.frame3)
        self.hazmat_thresh_txt = tk.StringVar(value='0.70')
        self.label41.configure(text='0.70', textvariable=self.hazmat_thresh_txt)
        self.label41.grid(column='2', row='3')
        self.label42 = ttk.Label(self.frame3)
        self.fire_thresh_txt = tk.StringVar(value='0.70')
        self.label42.configure(text='0.70', textvariable=self.fire_thresh_txt)
        self.label42.grid(column='2', row='4')
        self.frame3.configure(height='200', width='200')
        self.frame3.pack(anchor='n', fill='x', side='top')
        self.labelframe3.configure(height='240', text='Confidence threshold', width='200')
        self.labelframe3.pack(anchor='n', fill='x', ipadx='5', ipady='5', padx='5', pady='5', side='top')
        self.labelframe4 = ttk.Labelframe(self.toplevel1)
        self.frame5 = ttk.Frame(self.labelframe4)
        self.label43 = ttk.Label(self.frame5)
        self.label43.configure(text='Person')
        self.label43.grid(column='0', row='1')
        self.label44 = ttk.Label(self.frame5)
        self.label44.configure(text='Door')
        self.label44.grid(column='0', row='2')
        self.label45 = ttk.Label(self.frame5)
        self.label45.configure(text='Hazmat')
        self.label45.grid(column='0', row='3')
        self.label46 = ttk.Label(self.frame5)
        self.label46.configure(text='Fire Extinguisher')
        self.label46.grid(column='0', row='4')
        self.person_size_button = ttk.Menubutton(self.frame5)
        self.person_size_menu = tk.Menu(self.person_size_button)
        self.person_sz = tk.IntVar(value=160)
        self.mi_radiobutton4 = 0
        self.person_size_menu.add('radiobutton', label='160', value='160', variable=self.person_sz)
        self.mi_radiobutton5 = 1
        self.person_size_menu.add('radiobutton', label='192', value='192', variable=self.person_sz)
        self.mi_radiobutton1 = 2
        self.person_size_menu.add('radiobutton', label='224', value='224', variable=self.person_sz)
        self.mi_radiobutton2 = 3
        self.person_size_menu.add('radiobutton', label='256', value='256', variable=self.person_sz)
        self.mi_radiobutton3 = 4
        self.person_size_menu.add('radiobutton', label='288', value='160', variable=self.person_sz)
        self.mi_radiobutton6 = 5
        self.person_size_menu.add('radiobutton', label='320', value='320', variable=self.person_sz)
        self.person_size_menu.configure(tearoff='false')
        self.person_size_button.configure(text='Size', textvariable=self.person_sz)
        self.person_size_button.grid(column='1', row='1')
        self.door_size_button = ttk.Menubutton(self.frame5)
        self.door_size_menu = tk.Menu(self.door_size_button)
        self.door_sz = tk.IntVar(value=160)
        self.mi_radiobutton7 = 0
        self.door_size_menu.add('radiobutton', label='160', value='160', variable=self.door_sz)
        self.mi_radiobutton16 = 1
        self.door_size_menu.add('radiobutton', label='192', value='192', variable=self.door_sz)
        self.mi_radiobutton17 = 2
        self.door_size_menu.add('radiobutton', label='224', value='224', variable=self.door_sz)
        self.mi_radiobutton18 = 3
        self.door_size_menu.add('radiobutton', label='256', value='256', variable=self.door_sz)
        self.mi_radiobutton19 = 4
        self.door_size_menu.add('radiobutton', label='288', value='160', variable=self.door_sz)
        self.mi_radiobutton20 = 5
        self.door_size_menu.add('radiobutton', label='320', value='320', variable=self.door_sz)
        self.door_size_menu.configure(tearoff='false')
        self.door_size_button.configure(text='Size', textvariable=self.door_sz)
        self.door_size_button.grid(column='1', row='2')
        self.hazmat_size_button = ttk.Menubutton(self.frame5)
        self.hazmat_size_menu = tk.Menu(self.hazmat_size_button)
        self.hazmat_sz = tk.IntVar(value=320)
        self.mi_radiobutton11 = 0
        self.hazmat_size_menu.add('radiobutton', label='320', value='320', variable=self.hazmat_sz)
        self.mi_radiobutton27 = 1
        self.hazmat_size_menu.add('radiobutton', label='480', value='480', variable=self.hazmat_sz)
        self.mi_radiobutton12 = 2
        self.hazmat_size_menu.add('radiobutton', label='640', value='640', variable=self.hazmat_sz)
        self.hazmat_size_menu.configure(tearoff='false')
        self.hazmat_size_button.configure(text='Size', textvariable=self.hazmat_sz)
        self.hazmat_size_button.grid(column='1', row='3')
        self.fire_size_button = ttk.Menubutton(self.frame5)
        self.fire_size_menu = tk.Menu(self.fire_size_button)
        self.fire_sz = tk.IntVar(value=160)
        self.mi_radiobutton13 = 0
        self.fire_size_menu.add('radiobutton', label='160', value='160', variable=self.fire_sz)
        self.mi_radiobutton22 = 1
        self.fire_size_menu.add('radiobutton', label='192', value='192', variable=self.fire_sz)
        self.mi_radiobutton23 = 2
        self.fire_size_menu.add('radiobutton', label='224', value='224', variable=self.fire_sz)
        self.mi_radiobutton24 = 3
        self.fire_size_menu.add('radiobutton', label='256', value='256', variable=self.fire_sz)
        self.mi_radiobutton25 = 4
        self.fire_size_menu.add('radiobutton', label='288', value='160', variable=self.fire_sz)
        self.mi_radiobutton26 = 5
        self.fire_size_menu.add('radiobutton', label='320', value='320', variable=self.fire_sz)
        self.fire_size_menu.configure(tearoff='false')
        self.fire_size_button.configure(text='Size', textvariable=self.fire_sz)
        self.fire_size_button.grid(column='1', row='4')
        self.frame5.configure(height='200', width='200')
        self.frame5.pack(anchor='n', fill='x', side='top')
        self.labelframe4.configure(height='240', text='Inference size', width='200')
        self.labelframe4.pack(anchor='n', fill='x', ipadx='5', ipady='5', padx='5', pady='5', side='top')
        self.toplevel1.configure(height='480', width='1100')
        self.toplevel1.geometry('320x200')
        self.toplevel1.minsize(1100, 480)
        self.toplevel1.resizable(True, False)
        self.toplevel1.title('Object Detection')

        # Main widget
        self.mainwindow = self.toplevel1
    
    def run(self):
        self.mainwindow.mainloop()

    def on_person_conf(self, scale_value):
        pass

    def on_door_conf(self, scale_value):
        pass

    def on_hazmat_conf(self, scale_value):
        pass

    def on_fire_conf(self, scale_value):
        pass


if __name__ == '__main__':
    app = UiTemplateApp()
    app.run()

