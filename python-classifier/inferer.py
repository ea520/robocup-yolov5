from inference_data import inference_data
from model_types import model_type
from inference_models import model, qr_model, yolo_model
import cv2
from threading import Thread, Lock
import time, random
import numpy as np
from PIL import ImageDraw, Image
import warnings
warnings.filterwarnings('ignore', 'User provided device_type of \'cuda\', but CUDA is not available. Disabling')

def get_model(_type: model_type) -> model:
    """
    Load a model of a given type. Several models of the same type can be loaded but would be a waste of memory
    The model can then be used for inference
    """
    def _get_dir(name):
        return '../trained-models/' + name + '/weights/best.pt'
    
    if _type == model_type.QR:
        return qr_model()
    elif _type == model_type.PERSON:
        return yolo_model(model_type.PERSON, _get_dir("person"), inference_size=320)
    elif _type == model_type.DOOR:
        return yolo_model(model_type.DOOR, _get_dir("door"), inference_size=320)
    elif _type == model_type.FIRE_EXTINGUISHER:
        return yolo_model(model_type.FIRE_EXTINGUISHER, _get_dir("fire-extinguisher"), inference_size=320)
    elif _type == model_type.HAZMAT:
        return yolo_model(model_type.HAZMAT, _get_dir("hazmat"), inference_size=640)
    else:
        raise ValueError("Make sure every model type is listed here")


class inferer:
    """
    This does all of the work. Once instantiated, it will work in the background. 
    To terminate, use the stop() method.
    
    It has 2 main jobs:
     - Get frames from the camera
     - Perform inference on the camera feed
     - Usage is best shown by the example at the bottm of the file
    """
    def __init__(self, types: 'list[model_type]', cap: cv2.VideoCapture, width: int=640) -> None:
        self.lock = Lock() # used for thread safety

        self._inferences = inference_data()
        self._cap = cap
        _, self._frame = self._cap.read()

        while self._frame is None: 
            _, self._frame = self._cap.read()
            print("waiting for webcam")
            time.sleep(0.5)
        
        self._frame = cv2.cvtColor(self._frame, cv2.COLOR_BGR2RGBA)
        h, w, _ = self._frame.shape
        self._cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self._cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(h/w * width))
        
        self._models = [get_model(type) for type in types]

        self._inference_times = dict((type, 0.0) for type in types)
        self._inference_intervals = dict((type, 0.0) for type in types)
        self._inference_conf = dict((type, 0.2) for type in types)
        self._inference_size = dict((type, 320) for type in types)
        
        self._threads = [
            Thread(target=self._constant_inference, args=(self._models[i], type)) for i, type in enumerate(types)
        ]

        self._threads.append(
            Thread(target=self._get_frames)
        )
        self.running = True

        for p in self._threads:
            p.start() # start getting frames and performing inferences
        

    def _get_frames(self) -> None:
        while(self.running):
            _, frame = self._cap.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)

            self.lock.acquire()
            self._frame = frame
            self.lock.release()

            time.sleep(33/1000)
    def draw(self, drawer: ImageDraw, width: int, height: int) -> None:
        self._inferences.draw(drawer, width, height)

    def get(self) -> None:
        while len(self._frame) == 0 and self.running:
            time.sleep(0.2)
            print("waiting for first inference...")
        
        self.lock.acquire()
        frame = self._frame.copy()
        self.lock.release()
        return frame
    
    def _constant_inference(self, _model: model, _type: model_type) -> None:
        max_inference_period = 4000 # infer every 1.6 seconds (1600 ms)
        min_inference_period = 500 # infer 2 times per second
        inference_period = max_inference_period # start inferring slowly. 
        # If you see an object, increase the rate of inference
        # This reduces cpu time
        time.sleep(random.random()*inference_period/1000) # sleep for a random time so that the threads don't all infer at once
        while self.running:
            t0  = time.time()
            self.lock.acquire()
            frame = self._frame.copy()
            self.lock.release()
            _model.set_inference_size(self._inference_size[_type])
            data = _model.infer(frame, self._inference_conf[_type])
            if(data.empty):
                inference_period = min(max_inference_period, inference_period * 2)
            else:
                inference_period = max(min_inference_period, inference_period // 2)
            

            self.lock.acquire()
            self._inferences.replace(_type, data)
            self.lock.release()
            

            inference_time = time.time() - t0

            self.lock.acquire()
            self._inference_times[_type] = inference_time
            self._inference_intervals[_type] = inference_period / 1000.
            self.lock.release()
            if inference_time < inference_period/1000.: # convert ms to s
                time.sleep(inference_period/1000. - inference_time)
    
    def stop(self) -> None:
        self.running = False
        for p in self._threads:
            p.join()
    
    def set_conf(self, type: model_type, conf: float) -> None:
        self._inference_conf[type] = conf
    
    def set_inference_size(self,type: model_type, sz: int) -> None:
        self._inference_size[type] = sz
    
    def get_inference_interval(self, type: model_type) -> float:
        """
        Times are always in seconds when returned from this class 
        """
        return self._inference_intervals[type]

    def get_inference_time(self, type: model_type) -> float:
        """
        Times are always in seconds when returned from this class 
        """
        return self._inference_times[type]

if __name__ == "__main__":
    cap = cv2.VideoCapture(0)
    inf = inferer([model_type.QR, model_type.PERSON, model_type.DOOR, model_type.HAZMAT, model_type.FIRE_EXTINGUISHER], cap, width=640)
    while(True):
        frame = inf.get()
        img = Image.fromarray(frame)
        drawer = ImageDraw.Draw(img)
        inf.draw(drawer, img.width, img.height)
        open_cv_image = np.array(img)
        open_cv_image = cv2.cvtColor(open_cv_image, cv2.COLOR_RGB2BGR)
        cv2.imshow("Inferences", open_cv_image)
        if cv2.waitKey(10) in [27, ord('q')]: # stop when esc or q is clicked
            break
    # Closes all the frames
    cv2.destroyAllWindows()
    inf.stop()