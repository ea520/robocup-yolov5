from PIL import ImageTk, ImageDraw, Image
from uitemplate import UiTemplateApp
from inferer import inferer
from model_types import model_type
import warnings
import cv2
warnings.filterwarnings('ignore', 'User provided device_type of \'cuda\', but CUDA is not available. Disabling')

class App(UiTemplateApp):
    def __init__(self, cap, master=None):
        self.inferer = inferer([model_type.QR, model_type.PERSON, model_type.DOOR, model_type.HAZMAT, model_type.FIRE_EXTINGUISHER], cap, width=640)
        super().__init__(master=master)
        # Associate the menu buttons with their respective menus
        self.person_size_button["menu"] = self.person_size_menu
        self.door_size_button["menu"] = self.door_size_menu
        self.hazmat_size_button["menu"] = self.hazmat_size_menu
        self.fire_size_button["menu"] = self.fire_size_menu

        # Set the initial value of the image inference sizes
        self.person_sz.trace("w", self.person_sz_selected);self.person_sz.set(320)
        self.door_sz.trace("w", self.door_sz_selected);self.door_sz.set(320)
        self.hazmat_sz.trace("w", self.hazmat_sz_selected);self.hazmat_sz.set(320)
        self.fire_sz.trace("w", self.fire_sz_selected); self.fire_sz.set(320)

        # set the confidences to the ones on the screen
        self.on_person_conf(None)
        self.on_door_conf(None)
        self.on_hazmat_conf(None)
        self.on_fire_conf(None)

    def run(self):
        self.show_frame()
        self.mainwindow.mainloop()
    
    def person_sz_selected(self, *_):
        self.inferer.set_inference_size(model_type.PERSON,self.person_sz.get())
    def door_sz_selected(self, *_):
        self.inferer.set_inference_size(model_type.DOOR,self.door_sz.get())
    def hazmat_sz_selected(self, *_):
        self.inferer.set_inference_size(model_type.HAZMAT,self.hazmat_sz.get())
    def fire_sz_selected(self, *_):
        self.inferer.set_inference_size(model_type.FIRE_EXTINGUISHER,self.fire_sz.get())

    def show_frame(self):
        frame = self.inferer.get()
        img = Image.fromarray(frame)
        drawer = ImageDraw.Draw(img)
        self.inferer.draw(drawer, img.width, img.height)
        imgtk = ImageTk.PhotoImage(image=img)
        self.img_label.imgtk = imgtk
        self.img_label.configure(image=imgtk)

        _time = self.inferer.get_inference_time(model_type.PERSON)
        interval = self.inferer.get_inference_interval(model_type.PERSON)
        self.person_time_txt.set(f"{_time* 1000:05.1f} (ms) " + (":(" if _time > interval else ":)"))
        self.person_interval_txt.set(f"{interval* 1000:05.1f} (ms)")
        
        
        _time = self.inferer.get_inference_time(model_type.DOOR)
        interval = self.inferer.get_inference_interval(model_type.DOOR)
        self.door_time_txt.set(f"{_time* 1000:05.1f} (ms) " + (":(" if _time > interval else ":)"))
        self.door_interval_txt.set(f"{interval* 1000:05.1f} (ms)")

        _time = self.inferer.get_inference_time(model_type.HAZMAT)
        interval = self.inferer.get_inference_interval(model_type.HAZMAT)
        self.hazmat_time_txt.set(f"{_time* 1000:05.1f} (ms) " + (":(" if _time > interval else ":)"))
        self.hazmat_interval_txt.set(f"{interval* 1000:05.1f} (ms)")

        _time = self.inferer.get_inference_time(model_type.FIRE_EXTINGUISHER)
        interval = self.inferer.get_inference_interval(model_type.FIRE_EXTINGUISHER)
        self.fire_time_txt.set(f"{_time* 1000:05.1f} (ms) " + (":(" if _time > interval else ":)"))
        self.fire_interval_txt.set(f"{interval* 1000:05.1f} (ms)")


        _time = self.inferer.get_inference_time(model_type.QR)
        interval = self.inferer.get_inference_interval(model_type.QR)
        self.qr_time_txt.set(f"{_time* 1000:05.1f} (ms) " + (":(" if _time > interval else ":)"))
        self.qr_interval_txt.set(f"{interval* 1000:05.1f} (ms)")

        self.img_label.after(30, self.show_frame)
    
    def on_person_conf(self, _):
        scale_value = self.person_thresh.get()
        self.inferer.set_conf(model_type.PERSON, scale_value)
        self.person_thresh_txt.set(f"{scale_value:.2f}")

    def on_door_conf(self, scale_value):
        scale_value = self.door_thresh.get()
        self.inferer.set_conf(model_type.DOOR, scale_value)
        self.door_thresh_txt.set(f"{scale_value:.2f}")

    def on_hazmat_conf(self, scale_value):
        scale_value = self.hazmat_thresh.get()
        self.inferer.set_conf(model_type.HAZMAT, scale_value)
        self.hazmat_thresh_txt.set(f"{scale_value:.2f}")

    def on_fire_conf(self, scale_value):
        scale_value = self.fire_thresh.get()
        self.inferer.set_conf(model_type.FIRE_EXTINGUISHER, scale_value)
        self.fire_thresh_txt.set(f"{scale_value:.2f}")
    
if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    app = App(cap)
    app.run()
    app.inferer.stop()