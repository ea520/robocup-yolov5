import torch
from inference_data import qr_inference_datum, yolo_inference_datum, inference_datum
import numpy as np
from pyzbar import pyzbar
from abc import ABC, abstractmethod
from model_types import model_type
import pandas as pd

class model(ABC):
    """
    This class defines what methods are expected from an inference model
    Subclasses will fail to instantiate if these methods aren't defined
    """
    @abstractmethod
    def infer(self, img: np.ndarray, conf: float) -> inference_datum:
        pass

    @property
    @abstractmethod
    def type(self) -> model_type:
        pass
    
    @abstractmethod
    def set_inference_size(self, sz: int) -> None:
        # set the image size for inference in pixels (multiples of 32)
        pass

class yolo_model(model):
    def __init__(self, _type: model_type, path: str, inference_size:int=320) -> None:
        self._model = torch.hub.load('../yolov5/', 'custom', path=path, source="local")
        self._inference_size = inference_size
        self._type = _type
    def infer(self, img: np.ndarray, conf):
        self._model.conf = conf
        results = self._model(img, size=self._inference_size).pandas().xywhn[0] #type: pd.DataFrame
        return yolo_inference_datum(results, self.type)  
    @property
    def type(self):
        return self._type
    
    def set_inference_size(self, sz: int) -> None:
        self._inference_size = sz
        

class qr_model(model):
    def infer(self, img: np.ndarray, conf=None):
        codes = pyzbar.decode(img)
        frame_height, frame_width, _ = img.shape
        return qr_inference_datum(codes,frame_width, frame_height)
    @property
    def type(self):
        return model_type.QR

    def set_inference_size(self, sz: int) -> None:
        return