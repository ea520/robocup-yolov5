from abc import ABC, abstractmethod
import pandas as pd
from pyzbar import pyzbar
from model_types import model_type
from PIL import ImageDraw

class inference_datum(ABC):
    """
    This class defines what methods are expected from an inference datum
    An inference datum for QR codes contains all of the information to draw
    all of the QR codes' bounding boxes onto an image

    Subclasses will fail to instantiate if these methods aren't defined
    >>> import cv2
    >>> import PIL

    >>> img_cv = cv2.imread("foo.jpg")
    >>> img_pil = Image.fromarray(frame)
    >>> drawer = ImageDraw.Draw(img_pil)

    >>> datum.draw(drawer, width=640, height=480) # draw the bounding box, label and possibly confidence
    >>> datum_type = datum.type # e.g. QR or FIRE_EXTINGUISHER etc. defined in model_types.py
    """
    @abstractmethod
    def draw(self, drawer: ImageDraw.ImageDraw, width: int, height: int) -> None:
        pass

    @property
    @abstractmethod
    def type(self) -> model_type: 
        pass

    @property
    @abstractmethod
    def empty(self) -> bool: 
        pass

class yolo_inference_datum(inference_datum):
    def __init__(self, pd_data: pd.DataFrame, _type: model_type) -> None:
        """
        pd_data is obtained as follows
            >>> import torch
            >>> model = torch.hub.load('/path/to/yolov5/', 'custom', path=/path/to/best.pt, source="local")
            >>> detection = model(img)
            >>> pd_data = detection.pandas().xywhn[0] 
        make sure to use normalised coordinates xywhn rather than pixel coordinates xywh
        """
        # These should be in normalised coordinates (between 0 and 1)
        n = 0 if pd_data.empty else len(pd_data["width"])
        self._wn = [pd_data["width"][i] for i in range(n)] # make a list of the normalised widths of the bounding boxes
        self._hn = [pd_data["height"][i] for i in range(n)]
        self._xn = [pd_data["xcenter"][i] for i in range(n)]
        self._yn = [pd_data["ycenter"][i] for i in range(n)]
        self._confidence = [pd_data["confidence"][i] for i in range(n)]
        self._name = [pd_data["name"][i] for i in range(n)]
        self._type = _type
        self._empty = (n == 0)
    
    def draw(self, drawer: ImageDraw.ImageDraw, width: int, height: int) -> None:        
        for i in range(len(self._wn)):
            w = int(self._wn[i] * width) # width of the bounding box in pixels
            h = int(self._hn[i] * height)
            x = int(self._xn[i] * width) # centre x coordinate in pixels
            y = int(self._yn[i] * height)
            
            top_left = (x - w//2, y - h//2)
            bottom_right = (x + w//2, y + h//2)
            drawer.rectangle((top_left, bottom_right), outline=(200, 0, 200), width=1)
            drawer.text(top_left, f"{self._name[i]} {self._confidence[i]:.2f}", fill=(200, 0, 200))
    @property
    def type(self) -> model_type:
        return self._type
    
    @property
    def empty(self) -> bool:
        return self._empty
    
class qr_inference_datum(inference_datum):
    def __init__(self, codes: 'list[pyzbar.Decoded]', img_width: int, img_height: int) -> None:
        """
        code can be obtained as follows:
        >>> from pyzbar import pyzbar
        >>> import cv2
        >>> img = cv2.imread("foo.png")
        >>> codes = pyzbar.decode(img)
        >>> height, width, _ = img.shape
        """
        n = len(codes)
        self._x = [codes[i].rect[0] / img_width for i in range(n)]
        self._y = [codes[i].rect[1] / img_height for i in range(n)]
        self._w = [codes[i].rect[2] / img_width for i in range(n)]
        self._h = [codes[i].rect[3] / img_height for i in range(n)]
        self._labels = [codes[i].data.decode('utf-8') for i in range(n)]
        self._type = model_type.QR
        self._empty = (n==0)
    
    def draw(self, drawer: ImageDraw.ImageDraw, width: int, height: int) -> None:
        for i in range(len(self._x)):
            x = int(self._x[i] * width)
            y = int(self._y[i] * height)
            w = int(self._w[i] * width)
            h = int(self._h[i] * height)
            drawer.rectangle([(x,y),(x+w, y+h)], outline=(0, 200, 200), width=1)
            drawer.text((x,y), self._labels[i], fill=(0, 200, 200))

    @property
    def type(self) -> str:
        return self._type
    
    @property
    def empty(self) -> bool:
        return self._empty
    

class inference_data:
    """
    This class stores all the data required for rendering the bounding boxes etc.
    It is used to cache the data so the bounding boxes can be drawn every frame
    Without having to recalculate all of them for every frame
    """
    def __init__(self) -> None:
        self._data = [] # type: list[inference_datum]
    
    def replace(self, _type: model_type, data: inference_datum) -> None:
        """
        Remove the inference datum corresponding to the type and replace it with the new inference datum
        """
        self.remove(_type) # e.g. remove all QR codes from the list in preparation to add the new ones
        self._data.append(data) # append the data list
    
    def remove(self, _type: model_type) -> None:
        self._data = [dat for dat in self._data if dat.type != _type]
    
    def draw(self, drawer: ImageDraw.ImageDraw, width: int, height: int) -> None:
        for datum in self._data: 
            datum.draw(drawer, width, height)